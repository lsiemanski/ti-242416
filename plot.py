import matplotlib
import matplotlib.pyplot as plt
import io
import base64

def plot(times, time):
    img = io.BytesIO()

    plt.title("Zależność twojej liczby punktów od czasu")
    plt.xlabel("Czas")
    plt.ylabel("Punkty")

    points = len(times)
    plt.plot(times, [i for i in range(1, points+1)], 'ro')
    plt.axis([0, time, 0, points+1])

    plt.savefig(img, format='png')
    img.seek(0)
    url = base64.b64encode(img.getvalue()).decode()
    plt.close()

    return 'data:image/png;base64,{}'.format(url)