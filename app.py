from flask import Flask, render_template, session, redirect, request, jsonify
from lose_logic import lose_sign, lose_values, check_answer
from plot import plot

app = Flask(__name__)
app.secret_key = b'_5$y2L"Fff8z\n\xec]/'

LEVELS = ["easy", "medium", "hard"]
TIME_FOR_LEVEL = {"easy":60, "medium":90, "hard":120}

@app.route('/')
def index():
    return render_template("index.html")

@app.route('/game/<level>/')
def game(level):
    session.clear()
    session["good_answers_time"] = []
    session["level"] = level

    if level not in LEVELS:
        return redirect('/')

    time = TIME_FOR_LEVEL[level]

    return render_template("game.html", level=level, time=time)

@app.route('/lose/<level>/')
def lose(level):

    if level not in LEVELS:
        return ""

    sign = lose_sign()
    a, b = lose_values(sign, level)

    return render_template("operation.html", a=a, b=b, sign=sign)

@app.route('/check/')
def check():
    sign = request.args.get('sign')
    a = int(request.args.get('a'))
    b = int(request.args.get('b'))
    answer = int(request.args.get('answer'))
    time = int(request.args.get('time'))

    if check_answer(sign, a, b, answer):
        good_answers = session["good_answers_time"]
        good_answers.append(TIME_FOR_LEVEL[session["level"]] - time)
        session["good_answers_time"] = good_answers
        return jsonify(result=True)

    return jsonify(result=False)

@app.route('/author/')
def author():
    return render_template("author.html")

@app.route('/result/')
def result():
    return render_template("result.html", plot=plot(session["good_answers_time"], TIME_FOR_LEVEL[session["level"]]), points=len(session["good_answers_time"]), level=session["level"])