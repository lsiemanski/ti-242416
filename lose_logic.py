import random

ADD_SIGN = '+'
MULTIPLY_SIGN = '*'

RANGE = {
        ADD_SIGN: {
                "easy" : (11, 99),
                "medium": (101, 9999),
                "hard": (10001, 99999)
            },
        MULTIPLY_SIGN: {
                "easy": (11, 20),
                "medium": (11, 99),
                "hard": (101, 999)
            }
         }

def lose_sign():
    if random.random() > 0.5:
        return ADD_SIGN
    return MULTIPLY_SIGN

def lose_values(sign, level):
    return (random.randint(RANGE[sign][level][0], RANGE[sign][level][1]),
            random.randint(RANGE[sign][level][0], RANGE[sign][level][1]))

def check_answer(sign, a, b, answer):
    if sign == ADD_SIGN:
        if a + b == answer:
            return True
    else:
        if a*b == answer:
            return True
    return False